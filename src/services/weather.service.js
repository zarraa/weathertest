import constant from '../constant/index'
import React from 'react';

export default function fetchWeather(city)
{

  return   fetch(`${constant.baseUrl}weather?q=${city}&appid=${constant.secretKey}` ).then(res => res.json())


}
