import {StatusBar} from 'expo-status-bar';
import {StyleSheet, Text, View, Image, ImageBackground} from 'react-native';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import styleHome from './home.style'
import React from 'react';
import fetchWeather from "../../services/weather.service";

export default class HomeScreen extends React.Component {

    state = {
        humidity: null,
        temperature: 0,
        weatherCondition: null,
        description: null,
        error: null,
        getCurrentDate: null
    };

    async componentDidMount() {
        fetchWeather('Sousse,Tunis').then(data => {
            console.log(data)
            this.setState({
                temperature: (data.main.temp/10).toFixed(0),
                humidity: data.main.humidity,
                weatherCondition: data.weather[0].main,
                description: data.weather[0].description,
                getCurrentDate: this.getCurrentDate()
            });
        });


    }

    getCurrentDate = () => {

        const date = new Date().getDate();
        const month = new Date().getMonth() + 1;
        const year = new Date().getFullYear();
        return date + '/' + month + '/' + year;
    }

    render() {

        const {getCurrentDate, weatherCondition, temperature, description, humidity} = this.state
        const image=require(`../../../assets/Clear.jpg`)
       // const image=require(`../../../assets/${weatherCondition}.jpg`)
        return (

            <View style={styleHome.weatherContainer}>

                <ImageBackground source={image} resizeMode="cover"
                                 style={styleHome.image}>


                    <View style={styleHome.headerContainer}>
                        <MaterialCommunityIcons size={55} name="weather-sunset" color={'#fefefe'}/>
                        <Text style={styleHome.tempText}>Sousse </Text>
                        <Text style={styleHome.tempText}> {getCurrentDate}</Text>

                    </View>
                    <View style={styleHome.bodyContainer}>
                        <Text style={styleHome.subtitle}>Temperature˚ {temperature}</Text>
                        <Text style={styleHome.subtitle}>humidity˚ {humidity} %</Text>

                        <Text style={styleHome.title}>{weatherCondition}</Text>
                        <Text style={styleHome.subtitle}>{description}</Text>

                    </View>
                </ImageBackground>
            </View>
        );
    }
}


