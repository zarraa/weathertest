import {StyleSheet} from "react-native";

const styleHome = StyleSheet.create({
    weatherContainer: {
        flex: 1,
    },
    headerContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    tempText: {
        fontSize: 45,
        color: '#ffffff'
        // color: '#0854e1'
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    bodyContainer: {
        flex: 2,
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        paddingLeft: 25,
        marginBottom: 100
    },
    title: {
        fontSize: 60,
        color: '#ffffff'
    },
    subtitle: {
        fontSize: 24,
        color: '#ffffff'
    },
    time: {
        fontSize: 38,
        color: '#f1f1f1'
    }
});
export default styleHome
